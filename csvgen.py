from tinydb import TinyDB, Query
import dropbox
import datetime
import random

def get_csv_kit(fields):
    header_line = ""
    format_line = ""

    for i, (field, f_type) in enumerate(fields):
        if i > 0:
            format_line += ','
            header_line += ','
        
        header_line += field

        include_parenthesis = int(f_type == "str")

        tokens = field.split('.')

        if len(tokens) == 1:
            format_line += "\""*include_parenthesis + "{" + field + "}" + "\""*include_parenthesis
            continue
        
        format_line += "\""*include_parenthesis + "{" + tokens[0]

        for token in tokens[1:]:
            format_line += "[" + token + "]"

        format_line += "}" + "\""*include_parenthesis
    
    return header_line, format_line

def get_csv(db):
    reports_q = Query()

    reports = db.search(reports_q.dbtype == "report")

    header_line, format_line = get_csv_kit([
        ("cases.total", "int"),
        ("cases.student", "int"),
        ("cases.employee", "int"),
        ("quarantine.total", "int"),
        ("timestamp", "int"),
        ("cases.meta.weekday", "str"),
        ("cases.meta.date", "str")
    ])

    content = ""

    content += header_line + "\n"
    
    for report in reports:
        content += format_line.format(**report) + "\n"

    return content

def upload_csv(byte_content, api_key):
    dblink = dropbox.Dropbox(api_key)

    upload_location = f"/{datetime.date.today().isoformat()}-{random.randint(1000, 9999)}.csv"
    
    try:
        dblink.files_upload(byte_content, upload_location)
    except dropbox.exceptions.ApiError as err:
        print(f"Error uploading csv : {err}")
        return 0
    
    try:
        share_url = dblink.sharing_create_shared_link(upload_location).url
    except dropbox.exceptions.ApiError as err:
        print(f"Error getting share url : {err}")
        return 0

    return share_url

def dropbox_csv(config):
    csv_content = get_csv(config.database)
    csv_bytes = bytes(csv_content, 'utf8')

    csv_url = upload_csv(csv_bytes, config.dropbox_key)

    return csv_url
    