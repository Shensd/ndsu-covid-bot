import requests

class DiscordWebhook():
    def __init__(self, url):
        self.url = url

    def send_message(self, text):
        r = requests.post(self.url, data={
            "content" : text
        })

        return r
    
    def send_embed(self, embed):
        r = requests.post(self.url, json={
            "embeds" : [embed]
        })

        return r

    def send_embeds(self, embeds):
        r = requests.post(self.url, json={
            "embeds" : embeds
        })

        return r