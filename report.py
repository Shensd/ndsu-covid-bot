from dataclasses import dataclass
import requests
import re
from bs4 import BeautifulSoup
import datetime

@dataclass
class Cases:
    total : int
    student : int
    employee : int
    report_time : str
    report_weekday : str
    report_date : str

@dataclass
class Quarantine:
    total : int

@dataclass
class Report:
    cases_raw : str
    quarantine_raw : str
    cases : Cases
    quarantine : Quarantine

def get_raw_page(url):
    r = requests.get(url)

    if not r.ok:
        return None

    return r.text

def parse_cases(case_text):
    # note: for some reason sometimes the website uses spaces and other times it uses
    # characters that look like spaces but aren't actually spaces, so whitespace 
    # operators need to be used in the regex
    #
    # groups
    # 1 - total cases
    # 2 - student cases
    # 3 - employee cases
    # 4 - time of report
    # 5 - weekday of report
    # 6 - date of report
    case_match = re.compile(r"(\d+)\stotal\sconfirmed\spositive\scases\s\((\d+)\sstudents?\sand\s(\d+)\semployees?\)\sreported\sto\sNDSU\sin\sthe\spast\stwo\sweeks\sas\sof\s(\d+\s[ap]\.m\.),\s(\w+),\s([\w\d\s]+)\.")
    data = case_match.match(case_text)

    if data:
        return Cases(
            data.group(1), # total 
            data.group(2), # student
            data.group(3), # employee
            data.group(4), # report time
            data.group(5), # report weekday
            data.group(6)) # report date
    return None

def parse_quarantine(quar_text):
    # groups
    # 1 - total quarantined
    # quar_match = re.compile(r"(?:There\sare\s)?(\d+)\sstudents\s(?:are)?\scurrently\sin\squarantine\sin\suniversity\shousing\.")
    quar_match = re.compile(r"\sOf\sthe\sstudents,\s(\d+)\sare\sin\sisolation\sin\suniversity\shousing\sand\s(\d+)\sstudents\sare\scurrently\sin\squarantine\sin\suniversity\shousing.")
    
    data = quar_match.match(quar_text)

    if data:
        return Quarantine(
            data.group(2) # total 
        )
    return None

def get_report(raw_page):
    soup = BeautifulSoup(raw_page, 'html.parser')

    # magic class grabbing, there are a number of divs on the page that can be identified like this,
    # but the first is the one with the relevant text
    report_div = soup.findAll("div", {"class": "field field--name-body field--type-text-with-summary field--label-hidden field--item"})[0]

    # first two paragraph elements have case and quarantine text, respectively
    case_text = report_div.find_all('p')[0].text
    # quar_text = report_div.find_all('p')[1].text
    quar_text = ".".join(case_text.split(".")[3:])

    parsed_cases = parse_cases(case_text)
    parsed_quar  = parse_quarantine(quar_text)

    if not parsed_cases or not parsed_quar:
        return None

    return Report(
        case_text, 
        quar_text,
        parsed_cases,
        parsed_quar
    )

def report_to_obj(report : Report):
    return {
        "cases" : {
            "total" : report.cases.total,
            "student" : report.cases.student,
            "employee" : report.cases.employee,
            "meta" : {
                "time" : report.cases.report_time,
                "weekday" : report.cases.report_weekday,
                "date" : report.cases.report_date
            },
            "raw" : report.cases_raw
        },
        "quarantine" : {
            "total" : report.quarantine.total,
            "raw" : report.quarantine_raw
        },
        "timestamp" : datetime.datetime.now().timestamp(),
        "dbtype" : "report" 
    }
