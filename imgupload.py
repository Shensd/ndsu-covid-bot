import requests
import os

# return image url
def upload_image(file_path, url, key):
    if not os.path.exists(file_path):
        raise Exception("Given file does not exist.")
        return

    response = requests.post(
        url, 
        files={
            "f" : open(file_path,"rb")
        },
        headers={
            "Authorization" : key
        })

    if response.ok:
        return response.json()["url"]

    raise Exception(f"Image upload responded with error code {response.status_code}")

    return