from tinydb import TinyDB, Query
import os
import datetime
import base64

from config import Config, get_config
import graffiti
import imgupload
from report import get_report, report_to_obj, get_raw_page
from csvgen import dropbox_csv

PAGE_URL = "https://www.ndsu.edu/covid19/cases"
CONFIG = "./config.json"

def main():
    config = get_config(CONFIG)

    # if on an ignore day, don't run
    weekday = datetime.date.today().weekday()
    if weekday in config.ignore_days:
        return

    page_text = get_raw_page(PAGE_URL)

    if not page_text:
        print("Unable to fetch page.")
        return

    report = get_report(page_text)

    if not report:
        print("Unable to parse report.")
        return

    dict_report = report_to_obj(report)

    query = Query()
    today_entries = config.database.search(query.cases.meta.date == report.cases.report_date)

    # only insert new data if date isn't already in database
    if len(today_entries) == 0:
        config.database.insert(dict_report)

    reports = config.database.search(query.dbtype == 'report')

    image_path, exp_estimation, lin_estimation = graffiti.get_plot(reports, config.image_dir)
    image_url = imgupload.upload_image(image_path, config.upload_url, config.upload_key)

    dropbox_url = dropbox_csv(config)

    if dropbox_url == 0:
        print("Error uploading to dropbox")
        return

    for webhook in config.webhooks:
        response = webhook.send_embeds([{
            "title": "Daily Report",
            "color": 16517797,
            "description": f"[Updated CSV Link]({dropbox_url})",
            "fields": [
                {
                    "name": f"{report.cases.total} Total Cases",
                    "value": f"{report.cases.student} Student, {report.cases.employee} Employee\n{report.quarantine.total} in Quarantine"
                },
                {
                    "name" : f"{exp_estimation} exponential/{lin_estimation} linear Projected Cases For {(datetime.date.today() + datetime.timedelta(days=1)).isoformat()}",
                    "value" : "Estimated via regression on the last two weeks of data, (x=day,y=cases)\nExponential - fit `(log(13000 - y) - log(y)) ~ Ax + B`, then plot `e^(Ax + B)`\nLinear - fit `y ~ Ax + B`, then plot `Ax + B`"
                },
                {
                    "name": "Meta",
                    "value": f"Cases above total last 2 weeks up to {report.cases.report_time} {report.cases.report_weekday}, {report.cases.report_date}"
                }
            ],
            "author": {
                "name": "NDSU COVID-19 Case Tracker",
                "url": "https://www.ndsu.edu/covid19/cases",
                "icon_url": "https://www.publicdomainpictures.net/pictures/310000/velka/pink-circle.png"
            },
            "image" : {
                "url" : image_url
            }
        }])

        # print(response)
        # print(response.text)

if __name__ == "__main__":
    main()