from dataclasses import dataclass
from typing import List
from tinydb import TinyDB
import json

from discord_webhook import DiscordWebhook

@dataclass
class Config:
    webhooks : List[DiscordWebhook]
    database : TinyDB
    image_dir : str
    upload_key : str
    upload_url : str
    ignore_days : List[int]
    dropbox_key : str

def get_database(filepath):
    return TinyDB(filepath, sort_keys=True)

def get_config(config_url):
    with open(config_url, 'r') as config_raw:
        loaded = json.load(config_raw)

        webhooks = [ DiscordWebhook(hook) for hook in loaded["webhooks"] ]

        return Config(
            webhooks,
            get_database(loaded["database"]),
            loaded["image_dir"],
            loaded["upload_key"],
            loaded["upload_url"],
            loaded["ignore_days"],
            loaded["dropbox_key"]
        )