import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import datetime
import math
from dataclasses import dataclass
from sklearn.linear_model import LinearRegression
import scipy

def get_report_date(report):
    """Gets the date from a given report

    Parameters:
        report - an individual daily report

    Returns:
        datetime object derived from report's timestamp
    """
    return datetime.date.fromtimestamp(report["timestamp"])

def report_to_datapoint(begin_date, report):
    """Converts a given report into a data point

    Parameters:
        begin_date - used to derive relative x position, represents first day
            of data collection
        report - an individual daily report
    
    Returns:
        a tuple of (x pos (time since start date), y pos (day's reported cases))
    """
    return (
        (get_report_date(report) - begin_date).days, 
        int(report["cases"]["total"])
    )

def reports_to_realcases(begin_date, reports):
    if len(reports) < 15:
        raise Exception("Must provide at least 14 case points")
        return 

    datapoints = []

    def generate_delta_list(reports):
        deltas = []

        for i, report in enumerate(reports[1:]):
            deltas.append((
                get_report_date(report),
                int(report["cases"]["total"]) - int(reports[i-1]["cases"]["total"])
            ))

        for i, delta in enumerate(deltas):
            delta_date = delta[0]

            if (delta_date - begin_date) >= datetime.timedelta(days=14):

                target_date = delta_date - datetime.timedelta(days=14)

                for subdelta in deltas[:i]:
                    
                    subdelta_date = subdelta[0]

                    if abs(target_date - subdelta_date) < datetime.timedelta(days=1):
                        delta_cases = delta[1]

                        deltas[i] = (
                            delta_date,
                            delta_cases + subdelta[1]
                        )

                        print(f"adjusted delta, {delta_cases} -> {delta_cases + subdelta[1]}")
                        break

        return deltas

    case_deltas = generate_delta_list(reports)

    print(case_deltas)

    def get_offset_report(report):
        report_date = get_report_date(report)

        two_weeks_prior = report_date - datetime.timedelta(days=14)

        for i, test_delta in enumerate(case_deltas):

            time_difference = test_delta[0] - two_weeks_prior

            if abs(time_difference) < datetime.timedelta(days=1):
                return case_deltas[:i+1]

        return None

    for report in reports[14:]:
        # print(report["cases"]["meta"]["date"])
        offsets = get_offset_report(report)
        if offsets == None:
            continue

        total_offset = sum([ x[1] if x[1] >= 0 else 0 for x in offsets ])

        datapoints.append((
            (get_report_date(report) - begin_date).days,
            int(reports[14]["cases"]["total"]) + total_offset
        ))

    return datapoints


def optimize(fit_func, x, y):
    """Given a fit function and a set of x and y points, attempts to find an A and B
    that can fit the given points to the given function

    Parameters:
        fit_func - fit function in the form of (t, a, b) -> result, where x is used
            for t and mapped to the corresponding y
        x - set of x points
        y - set of y points

    Returns:
        the optimized A and B parameters
    """
    optimized = scipy.optimize.curve_fit(fit_func,  x,  y)
    A, B = optimized[0][0], optimized[0][1]

    return (A, B)

def process_y(y_func, x):
    """Given a function and a set of x's, returns corresponding y's

    Parameters:
        y_func - function to plug x into and take result
        x - set of x points

    Returns:
        set of corresponding y points
    """
    return [ y_func(x1) for x1 in x ]

def get_plot(reports, image_dir):
    """given a set of daily reports and an image directory, generates a graph
    with predictions, and saves the image to the given directory

    Parameters:
        reports - a set of daily case reports
        image_dir - directory to save output image to

    Returns:
        file path to generated image
    """
    if len(reports) < 2:
        raise Exception("Must provide at least 2 case points")
        return

    # sort reports by date and grab the first report as start date
    reports.sort(key=lambda report:int(report["timestamp"]))
    begin_date = get_report_date(reports[0])

    datapoints = [ report_to_datapoint(begin_date, report) for report in reports ]

    real_points = reports_to_realcases(begin_date, reports)

    # fig, (ax1, ax2) = plt.subplots(nrows=2)
    fig, ax1 = plt.subplots()
    
    # only predict on last 14 days
    x = np.array([x for x,y in datapoints][-14:])
    y = np.array([y for x,y in datapoints][-14:])
    x_fit = np.linspace(np.min(x), np.max(x)+2, 100)

    population_size = 13000
    L = np.log(population_size - y) - np.log(y)
    A, B = optimize(
        lambda t, a, b: (a*t)+b,
        x, L
    )
    y_fit = process_y(
        lambda t: population_size / (1 + math.pow(math.e, (A*t)+B)),
        x_fit
    )
    ax1.plot(x_fit, y_fit, color="purple")
    exp_tomorrow_estimation = int(population_size / (1 + math.pow(math.e, (A*(np.max(x)+1))+B)))
    
    # plot linear regression
    # x = [days], y = [actual cases]
    # estimate A and B to fit y = Ax + B
    # then plot (days+3, A(day) + B)
    A, B = optimize(
        lambda t, a, b: (a*t)+b,
        x, y
    )
    y_fit = process_y(
        lambda t: (A*t)+B,
        x_fit
    )
    ax1.plot(x_fit, y_fit, color="green")
    lin_tomorrow_estimation = int((A*(np.max(x)+1))+B)

    # plot actual data
    ax1.plot([x for x,y in datapoints], [y for x,y in datapoints], color='red', marker='x', linewidth=2)

    # plot real cases
    # ax2.plot([x for x,y in real_points], [y for x,y in real_points], color='blue', marker='o', linewidth=1)

    ax1.set_xlabel("Time (Days)")
    ax1.set_ylabel("Daily Reported Cases")
    ax1.set_title("NDSU COVID-19 Daily Reported Cases Over Time")
    ax1.grid()
    ax1.xaxis.set_major_locator(ticker.MaxNLocator(integer=True))

    # ax2.set_xlabel("Time (Days)")
    # ax2.set_ylabel("Total Cases")
    # ax2.grid()

    fig.tight_layout()

    figname = f"{image_dir}/{datetime.date.today().isoformat()}.jpg"

    plt.savefig(figname, dpi=300)

    return (figname, exp_tomorrow_estimation, lin_tomorrow_estimation)